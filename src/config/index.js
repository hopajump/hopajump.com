module.exports = {
  title: 'Anisha has a blog called hopajump',
  description: 'The portfolio and blog of Anisha',
  keywords:
    'web designer for hire, designer for hire, Anisha design, oscar diaz, hopajump, Anisha Brooks',
  url: 'http://hopajump.com',
  image: 'http://hopajump.com/assets/favicon/android-chrome-512x512.png',
  logo: 'http://hopajump.com/assets/favicon/android-chrome-512x512.png',
  twitter: '@hopajump',
  fbAppID: '',

  // This is used for generating blog preview pages in `gatsby-node.js`.
  postsPerPage: 10,
}
